declare module "crypt" {
    function encrypt (data:string, custom_key: string) : string;
    function decrypt (data:string, custom_key: string): string;
    function gen_salt(length: number, callback : (err: Error, salt: string) => void): void;
}