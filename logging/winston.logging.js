import { createLogger, format, transports } from "winston";
import { Loggly } from "winston-loggly";
const logger = module.exports = createLogger({
    level: "info",
    format: format.combine(
        format.colorize({ all: true }),
        format.json(),
    ),
    defaultMeta: { service: "user-service" },
    transports: [
      new transports.File({ filename: "error.log", level: "error" }),
      new transports.File({ filename: "combined.log" }),
      Loggly({
        Loggly: {
          token: "YOUR_TOKEN",
          subdomain: "YOUR_SUBDOMAIN",
        },
      }),
    ],
  });
if (process.env.NODE_ENV !== "production") {
    logger.add(new transports.Console({
      format: format.simple(),
    }));
  }
