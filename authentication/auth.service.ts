
import { MessagingService } from '../messaging/messaging.service';
import { IJwtPayload } from './interfaces/jwt-payload.interface';

export class AuthService {
  constructor(
    // private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
  ) {}

  public async signIn(phoneNumber: number, otp: number): Promise<string | boolean> {
    const messagingService = new MessagingService(phoneNumber);
    return new Promise((resolve: (response: string) => void, reject: (error: Error) => void) => {
      if (messagingService.verifyOtp(otp)) {
        const user: IJwtPayload = { phone: phoneNumber };
        resolve(this.jwtService.sign(user));
      } else {
        reject(new Error('Invalid Otp'));
      }
    });
  }

  public async validateUser(payload: IJwtPayload): Promise<any> {
    // TODO: Make a valid validation method
    return true;
    // return await this.usersService.findOneByEmail(payload.phone);
  }
}
