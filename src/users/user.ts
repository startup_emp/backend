import {Sequelize} from 'sequelize-typescript';
import { User } from './models/user.model';

export const sequelize =  new Sequelize({
        database: 'dropdatabase',
        dialect: 'mysql',
        username: 'root',
        password: '',
});

sequelize.addModels([User]);
