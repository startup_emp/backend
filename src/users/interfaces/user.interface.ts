export interface UserInterface {
    name: string;
    id: string;
    phone: number;
    email: string;
    location: any;
}
