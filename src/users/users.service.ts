import { User, UserModel } from './models/user.model';

export class UsersService {
    /**
     * isValidUser
     */
    public async findUserByPhoneNumber(phoneNumber: number): Promise<User|null> {
        return new Promise((resolve: (response: User | null) => void, reject: (error: Error) => void) => {
            User.findOne({
                where: {
                    phone: phoneNumber
                }
            })
            .then(userObject => resolve(userObject))
            .catch(e => reject(e));
        });
    }

    public async addNewUser(phoneNumber: number): Promise<User> {
        return new Promise((resolve: (response: User) => void, reject: (error: Error) => void) => {
            User.create({
                phone: phoneNumber
            })
            .then(userObject => resolve(userObject))
            .catch(e => reject(e));
        });
    }
}
