import {Table, Column, Model} from 'sequelize-typescript';
import { DataTypes } from 'sequelize';

@Table({
    timestamps: false,
    freezeTableName: true
})
export class User extends Model<User> {

    @Column({ type: DataTypes.STRING, field: 'phone' })
    phoneNumber!: number;

    @Column(DataTypes.STRING)
    name!: string;

    @Column(DataTypes.STRING)
    email!: string;

    @Column(DataTypes.STRING)
    location!: number;
}

export interface UserModel {
    id: string;
    phoneNumber: number;
    name: string;
    email: string;
    location: number;
}
