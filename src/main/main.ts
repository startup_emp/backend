import express from 'express';
import { MainServices } from './main.controller';
import { UsersService } from '../users/users.service';
import { sequelize } from '../users/user';
import { json } from 'body-parser';
import { sign } from 'jsonwebtoken';
import { Promise as bluPromise} from 'bluebird';
import { SecurityConstants } from '../authentication/authentication.constants';
import { OrderController } from '../order/order.controller';

const app: express.Application = express();
const port = 3000;
// TODO: store ip in a const global variable or as request parameter
// TODO: store phoneNumber in jwt token in a const global variable or as request parameter
/*app.use(function(params:type) {
    'var ip = req.header('x-forwarded-for') || req.connection.remoteAddress;'
});*/
app.get('/areas', (req, res) => MainServices.getPincode(res));

app.get('/otp', (req, res) => {
    const mainServices = new MainServices();
    mainServices.getOtp(req.query.phone_number, res);
});

app.use(json());
app.post('/login', (req, res) => {
    const mainServices = new MainServices();
    mainServices.login(req.body.phone_number, req.body.message, res);
});

app.get('users/:phone_number', (req, res) => {
    const mainServices = new MainServices();
    const phoneNumber = req.params.phone_number;
    // TODO: check if this and the phone_number sent in the token are the same
    mainServices.getUserDetails(phoneNumber, res);
});

app.post('/orders/create', (req, res) => {
    const orderController = new OrderController();
    const phoneNumber: number = req.body.phone_number;
    const quantity: number = req.body.quantity;
    const deliveryTime: number = req.body.delivery_ime;
    orderController.createOrder(phoneNumber, quantity, deliveryTime, res);
});

app.post('/orders/:order_id/verify', (req, res) => {
    console.log('ver');
});

app.get('/orders/paytm/checksum', (req, res) => {
    const orderController = new OrderController();
    orderController.createChecksum()
});

// Test Methods - Start
app.get('/sign', (req, res) => {
    createToken();
});

async function createToken() {
    const bluSign = bluPromise.promisify(sign);
    const token = await bluSign({ phone_number: 123 }, SecurityConstants.secretKey);
    console.log(token);
}

app.get('/find', (req, res) => {
    new UsersService().findUserByPhoneNumber(741820993)
    .then(userObject => userObject && userObject.get({ plain: true }))
    .then(user => user ? res.send(user) : res.send('Not Found'))
    .catch(e => console.log(e));
});
// Test Methods - Ends

app.listen(port, () => {
    sequelize.sync();
    console.log(`Example app listening on port ${port}!`);
});
