import { Promise as bluebirdPromise, resolve } from 'bluebird';
import { Response } from 'express-serve-static-core';
import { readFile as callbackReadFile } from 'fs';
import { sign as callbackSign} from 'jsonwebtoken';

import { MessagingService } from '../messaging/messaging.service';
import { UsersService } from '../users/users.service';

import { UserInterface } from '../users/interfaces/user.interface';

import { ErrorConstants } from '../api/error.constants';
import { SecurityConstants } from '../authentication/authentication.constants';
import { StatusCodes } from '../api/status.codes';
import { SuccessResponse } from '../api/success.response';
import { ErrorResponse } from '../api/error.response';
import { ErrorTitles } from '../api/error-titles.enum';
import { OtpResponseInterface } from './interfaces/otp-response.interface';
import { LoginResponse } from './interfaces/loing-response.interface';
import { IS_NEW_USER } from './main.constants';

const readFile = bluebirdPromise.promisify(callbackReadFile);
export class MainServices {
  status = StatusCodes.OK;
  public static getPincode(res: Response): void {
    readFile('Res/pincode.json').then(file => {
      const areas: AreaResponseInterface[] = JSON.parse(file.toString());
      new SuccessResponse(StatusCodes.OK, areas).sendResponse(res);
    });
  }

  public async getOtp(phoneNumber: number, res: Response): Promise<void> {
    const requestUrl = '/otp';
    try {
      const messagingService = new MessagingService(phoneNumber);
      const message = await messagingService.sendOtp();
      const status = StatusCodes.OK;
      let isNewUser: boolean = true;
      try {
        const userObject = await new UsersService().findUserByPhoneNumber(phoneNumber);
        if (userObject) {
          const user = userObject.get({ plain: true }) as UserInterface;
          if (!user) {
            isNewUser = false;
          }
        } else {
          console.log('USER NOT FOUND');
          isNewUser = false;
        }
      } catch (e) {
        console.log(e);
      } finally {
        const response: OtpResponseInterface = {
          [IS_NEW_USER] : isNewUser
        };
        new SuccessResponse(status, response).sendResponse(res);
      }
    } catch (e) {
      const err: Error = e;
      switch (err.message) {
        case ErrorConstants.INVALID_PHONE_NUMBER:
          new ErrorResponse(requestUrl, ErrorTitles.INVALID_PHONE_NUMBER).sendResponse(res);
          break;
        default:
          new ErrorResponse(requestUrl, ErrorTitles.SERVER_SIDE_GENERIC_ERROR).sendResponse(res);
      }
    }
  }

  private async createToken(phoneNumber: number): Promise<string> {
    const sign = bluebirdPromise.promisify(callbackSign);
    return await sign({ phone_number: phoneNumber }, SecurityConstants.secretKey) as string;
  }

  public async login(phoneNumber: number, otp: number, res: Response): Promise<void> {
    const requestUrl = 'login';
    try {
      const messagingService = new MessagingService(phoneNumber);
      const isVerifiedUser = await messagingService.verifyOtp(otp); // only returns true, and throws error in false case
      if (isVerifiedUser) {
        const statusCode = StatusCodes.OK;
        const userService = new UsersService();
        let userObject = await userService.findUserByPhoneNumber(phoneNumber);
        if (!userObject) {
          userObject = await userService.addNewUser(phoneNumber);
        }
        const user = userObject.get({ plain: true }) as UserInterface;
        if (user) {
          const token = await this.createToken(phoneNumber);
          const response: LoginResponse = {token};
          new SuccessResponse(statusCode, response).sendResponse(res);
        } else {
          new ErrorResponse(requestUrl, ErrorTitles.SERVER_SIDE_GENERIC_ERROR).sendResponse(res);
        }
      }
    } catch (e) {
      const err: Error = e;
      console.log(e);
      switch (err.message) {
        case ErrorConstants.INVALID_PHONE_NUMBER:
          new ErrorResponse(requestUrl, ErrorTitles.INVALID_PHONE_NUMBER).sendResponse(res);
          break;
          // TODO: handle already_verified, invalid_otp cases from MessageService.verifyOtp
        default:
          new ErrorResponse(requestUrl, ErrorTitles.SERVER_SIDE_GENERIC_ERROR).sendResponse(res);
      }
    }
  }

  /*
    Functions after token authentication
  */
  // Move to User Route
  public async getUserDetails(phoneNumber: number, res: Response): Promise<void> {
    try {
      const userService = new UsersService();
      const userObject = await userService.findUserByPhoneNumber(phoneNumber);
      if (!userObject) { throw new Error('USER_NOT_FOUND'); }
      const response = userObject.get({ plain: true }) as UserInterface;
      new SuccessResponse(StatusCodes.OK, response).sendResponse(res);
    } catch (e) {
      const err: Error = e;
      console.log(e);
    }
  }
}
