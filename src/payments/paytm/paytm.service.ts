import Axios, { AxiosRequestConfig } from 'axios';

import { PaytmApiConstants, version, channelId, requestTimestamp, clientId, signature, requestType,
    merchantIdTesting, websiteName, amount, PaytmUserInforInterface, industryType, webSite } from './paytm.constatns';
import paytmConfig from './sdk/paytm_config';
import {genchecksum, verifychecksum} from './sdk/checksum';

import { UserInterface } from '../../users/interfaces/user.interface';
import { ChecksumInterface } from './interfaces/checksum.interface';

export class PaytmService {
    public async initiateTransaction(orderId: number, userDetails: UserInterface): Promise<string> {
        const userInfo: PaytmUserInforInterface = {
            custId: userDetails.id,
            mobile: userDetails.phone,
            email: userDetails.email,
            firstName: userDetails.name
        };
        const options: AxiosRequestConfig = {
            method: 'post',
            headers: {
                version,
                channelId,
                requestTimestamp,
                clientId,
                signature
            },
            data: {
                requestType,
                mid: merchantIdTesting,
                orderId,
                websiteName,
                txnAmount: amount,
                userInfo
            },
            url: PaytmApiConstants.initiateTransactionApiUrl
          };
        const response = await Axios.request(options);
        if (response.data.body.resultInfo.resultStatus !== 'S') {
            throw new Error(response.data.body.resultInfo.resultMsg);
        }
        return response.data.txnToken as string;
    }

    public async generateChecksum(data: ChecksumInterface): Promise<string> {
        const paramarray: any = {};
        paramarray.MID = merchantIdTesting;
        paramarray.ORDER_ID = orderId;
        paramarray.CUST_ID = userId;
        // paramarray.ORDER_ID = 'ORDER00001';
        // paramarray.CUST_ID = 'CUST0001';
        paramarray.INDUSTRY_TYPE_ID = industryType;
        paramarray.CHANNEL_ID = 'WAP'; // Provided by Paytm
        paramarray.TXN_AMOUNT = '1.00'; // transaction amount
        paramarray.WEBSITE = webSite;
        // paramarray.CALLBACK_URL = 'https://pguat.paytm.com/paytmchecksum/paytmCallback.jsp'; // Provided by Paytm
        paramarray.EMAIL = email;
        paramarray.MOBILE_NO = phoneNumber;
        return new Promise((resolve: (response: string) => void, reject: (error: Error) => void) => {
            genchecksum(paramarray, paytmConfig.MERCHANT_KEY, (err, checksum) => {
                if (err) {
                    console.log(err);
                    reject(err);
                }
                console.log('Checksum: ', checksum, '\n');
                resolve(checksum);
            });
        });
    }

    public async validateChecksum({ orderId, userId }: ChecksumInterface, checksum: string) {
        let paytmChecksum = '';
        const paytmParams = {};
        for (const key in data) {
            if (key === 'CHECKSUMHASH') {
                paytmChecksum = data[key];
            } else {
                paytmParams[key] = data[key];
            }
        }

        const isValidChecksum = verifychecksum(paytmParams, 'YOUR_KEY_HERE', paytmChecksum);
        if (isValidChecksum) {
            console.log('Checksum Matched');
        } else {
            console.log('Checksum Mismatched');
        }
    }
}
