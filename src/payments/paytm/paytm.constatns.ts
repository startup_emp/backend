// Header Constants
export const version = 'v1';
export const channelId = 'APP';
export const requestTimestamp = new Date().getTime();
export const clientId = ''; // ClientId by which key checksum is created, required to validate the checksum. Eg C11
export const signature: string = ''; // Checksum string created by using paytm checksum logic/library

// Body Constants
export const requestType = 'UNI_PAY';
export const industryType = 'Retail';
export const webSite = 'WEBSTAGING';

export const merchantIdTesting = 'knkYkw06010774211271';
export const merchantId = '';
export const secretKeyTesting = 'pB@i47&uJrtSm8qZ';
export const secretKey = '';

export const websiteName = 'https://dropithere.com';
export const amount = 35;

export class PaytmApiConstants {
    static readonly baseUrlTesting = 'https://securegw-stage.paytm.in';
    static readonly baseUrl = 'https://securegw.paytm.in';

    static readonly initiateTransactionApiUrl = PaytmApiConstants.baseUrlTesting + '/theia/api/' + version + '/initiateTransaction';
}

export interface PaytmUserInforInterface {
    custId: string;
    mobile: number;
    email: string;
    firstName: string;
}
