import { encrypt, decrypt, gen_salt } from './crypt';
import util from 'util';
import crypto from 'crypto';

const mandatoryParams: string[] = [];
// mandatory flag: when it set, only mandatory parameters are added to checksum
function paramsToString(params: { [x: string]: string; }, mandatoryflag: boolean) {
  let data = '';
  const tempKeys = Object.keys(params);
  tempKeys.sort();
  tempKeys.forEach(key =>  {
  const n = params[key].includes('REFUND');
  const m = params[key].includes('|');
  if (n === true ) {
          params[key] = '';
        }
  if (m === true) {
          params[key] = '';
        }
  if (key !== 'CHECKSUMHASH' ) {
      if (params[key] === 'null') { params[key] = ''; }
      if (!mandatoryflag || mandatoryParams.indexOf(key) !== -1) {
        data += (params[key] + '|');
      }
    }
});
  return data;
}

export function genchecksum(params: any, key: any, cb: (arg0: any, arg1: string) => void) {
  const data = paramsToString(params, false);
  gen_salt(4, (err: Error, salt: string) => {
    const sha256 = crypto.createHash('sha256').update(data + salt).digest('hex');
    const checkSum = sha256 + salt;
    const encrypted = encrypt(checkSum, key);
    cb(undefined, encrypted);
  });
}

export function verifychecksum(params: any, key: any, checksumhash: string) {
  const data = paramsToString(params, false);

  // TODO: after PG fix on thier side remove below two lines
  if (typeof checksumhash !== 'undefined') {
    checksumhash = checksumhash.replace('\n', '');
    checksumhash = checksumhash.replace('\r', '');
    const temp = decodeURIComponent(checksumhash);
    const checksum = decrypt(temp, key);
    const salt = checksum.substr(checksum.length - 4);
    const sha256 = checksum.substr(0, checksum.length - 4);
    const hash = crypto.createHash('sha256').update(data + salt).digest('hex');
    if (hash === sha256) {
      return true;
    } else {
      util.log('checksum is wrong');
      return false;
    }
  } else {
    util.log('checksum not found');
    return false;
  }
}

export function genchecksumbystring(datta, key, cb) {
  gen_salt(4, (err: Error, salt: string) => {
    const sha256 = crypto
      .createHash('sha256')
      .update(datta + '|' + salt)
      .digest('hex');
    const checkSum = sha256 + salt;
    const encrypted = encrypt(checkSum, key);

    const CHECKSUMHASH = encodeURIComponent(encrypted);
    CHECKSUMHASH = encrypted;
    cb(undefined, CHECKSUMHASH);
  });
}

export function verifychecksumbystring(params, key, checksumhash) {
  var checksum = decrypt(checksumhash, key);
  var salt = checksum.substr(checksum.length - 4);
  var sha256 = checksum.substr(0, checksum.length - 4);
  var hash = crypto
    .createHash('sha256')
    .update(params + '|' + salt)
    .digest('hex');
  if (hash === sha256) {
    return true;
  } else {
    util.log('checksum is wrong');
    return false;
  }
} 

export function genchecksumforrefund(params: { CHECKSUM: string; }, key: any, cb: (arg0: any, arg1: any) => void) {
  const data = paramsToStringrefund(params);
  gen_salt(4, (err: any, salt: string) => {
    const sha256 = crypto.createHash('sha256').update(data + salt).digest('hex');
    const checkSum = sha256 + salt;
    const encrypted = encrypt(checkSum, key);
    params.CHECKSUM = encodeURIComponent(encrypted);
    cb(undefined, params);
  });
}

function paramsToStringrefund(params: { [x: string]: string; }, mandatoryflag = false) {
  let data = '';
  const tempKeys = Object.keys(params);
  tempKeys.sort();
  tempKeys.forEach(key => {
   const m = params[key].includes('|');
   if (m === true) {
          params[key] = '';
        }
   if (key !== 'CHECKSUMHASH' ) {
      if (params[key] === 'null') { params[key] = ''; }
      if (!mandatoryflag || mandatoryParams.indexOf(key) !== -1) {
        data += (params[key] + '|');
      }
    }
});
  return data;
}

module.exports.genchecksum = genchecksum;
module.exports.verifychecksum = verifychecksum;
// module.exports.verifychecksumbystring = verifychecksumbystring;
// module.exports.genchecksumbystring = genchecksumbystring;
module.exports.genchecksumforrefund = genchecksumforrefund;
