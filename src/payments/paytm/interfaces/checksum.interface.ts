export interface ChecksumInterface {
    orderId: string;
    userId: string;
    merchantId?: string;
}
