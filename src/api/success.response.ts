import { ServerResponse, ResponseTypes, ResponseInterface } from './response';
import { StatusCodes } from './status.codes';
import { Response } from 'express-serve-static-core';
import { OtpResponseInterface } from '../main/interfaces/otp-response.interface';
import { LoginResponse } from '../main/interfaces/loing-response.interface';
import { UserInterface } from '../users/interfaces/user.interface';
import { OrderResponseInterface } from '../order/interface/order.interface';

type successResponseTypes = AreaResponseInterface[] | OtpResponseInterface | LoginResponse | UserInterface | OrderResponseInterface;

export interface SuccessResponseInterface extends ResponseInterface {
    response: successResponseTypes;
}

export class SuccessResponse extends ServerResponse {

    constructor(statusCode: StatusCodes, private response: successResponseTypes) {
        super(ResponseTypes.SUCCESS, statusCode);
    }

    public sendResponse(res: Response): void {
        const response: SuccessResponseInterface = {
            message: this.message,
            status_code: this.statusCode,
            response: this.response
        };
        res.status(this.statusCode).json(response);
    }
}
