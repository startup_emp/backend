import { StatusCodes } from './status.codes';
import { Response } from 'express-serve-static-core';

export enum ResponseTypes {
    SUCCESS = 'success',
    FAILURE = 'failure'
}

export interface ResponseInterface {
    message: ResponseTypes;
    status_code: StatusCodes;
}

export abstract class ServerResponse {

    constructor(protected message: ResponseTypes, protected statusCode: StatusCodes) {
        this.message = message;
        this.statusCode = statusCode;
    }

    public abstract sendResponse(res: Response): void;
}
