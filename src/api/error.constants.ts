export class ErrorConstants {
    public static readonly INVALID_PHONE_NUMBER = 'invalid_phone_number';
    public static readonly MESSAGE_FAILURE = 'message_sending_failure';
    public static ALREADY_VERIFIED = 'already_verified';
}
