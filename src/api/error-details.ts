import { StatusCodes } from './status.codes';
import { ErrorDetails } from './error.response';
import { ErrorTitles } from './error-titles.enum';

const errorDetails: ErrorDetails =  {
    [ErrorTitles.INVALID_PHONE_NUMBER]: {
        statusCode: StatusCodes.CLIENT_SIDE_GENTRIC_ERROR,
        detail: 'enter a valid phone number'
    },
    [ErrorTitles.SERVER_SIDE_GENERIC_ERROR]: {
        statusCode: StatusCodes.SERVER_SIDE_GENERIC_ERROR,
        detail: 'unknown server side error'
    },
    [ErrorTitles.UNEXPECTED_ERROR]: {
        statusCode: StatusCodes.SERVER_SIDE_GENERIC_ERROR,
        detail: 'could not find the route cause'
    },
};
export default errorDetails;
