import { UserInterface } from '../users/interfaces/user.interface';
import { StatusCodes } from './status.codes';

export interface APIResponseInterface {
    message: string;
    status: StatusCodes;
    is_new_user?: boolean;
    user_details?: UserInterface;
    token?: string;
}
