import { ErrorTitles } from './error-titles.enum';
import { ServerResponse, ResponseTypes, ResponseInterface } from './response';
import { StatusCodes } from './status.codes';
import { Response } from 'express-serve-static-core';
import errorDetails from './error-details';

export interface SubAPIErrorInterface {
    url: string;
    title: ErrorTitles;
    detail: string;
}

export interface ErrorResponseInterface extends ResponseInterface {
    error: SubAPIErrorInterface;
}

interface SubErrorDetails {
    statusCode: StatusCodes;
    detail: string;
}

export type ErrorDetails = {
    [key in ErrorTitles]: SubErrorDetails;
};

export class ErrorResponse extends ServerResponse {
    url: string;
    title: ErrorTitles;
    detail: string;

    constructor(url: string, title: ErrorTitles) {
        super(ResponseTypes.FAILURE, errorDetails[title].statusCode);
        this.url = url;
        this.title = title;
        this.detail = errorDetails[title].detail;
    }

    public sendResponse(res: Response) {
        const errorResponse: ErrorResponseInterface = {
            message: this.message,
            status_code: this.statusCode,
            error : {
                url : this.url,
                title: this.title,
                detail: this.detail
            }
        };
        res.status(this.statusCode).json(errorResponse);
    }
}
