export enum StatusCodes {
    /**
     * everything workded as expected & response will be returned
     */
    OK = 200,
    /**
     * resource is created & response will be returned
     */
    CREATED = 201,
    /**
     * everything workded as expected & response is not needed
     */
    NO_RESPONSE = 204,
    /**
     * to indicate generic client-side failure
     */
    CLIENT_SIDE_GENTRIC_ERROR = 400,
    /**
     * when there is a problem with the client’s credentials
     */
    UNAUTHORIZED = 401,
    /**
     * when the client attempts a resource interaction that is outside of its permitted scope
     * to enforce application-level permissions.
     * a client may be authorized to interact with some, but not this REST API’s resources.
     */
    FORBIDDEN = 403,
    /**
     * when a client’s URI cannot be mapped to a resource
     */
    NOT_FOUND = 404,
    /**
     * when the client execute some request handler code that raises an exception
     */
    SERVER_SIDE_GENERIC_ERROR = 500
}
