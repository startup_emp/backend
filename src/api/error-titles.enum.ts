export enum ErrorTitles {
    INVALID_PHONE_NUMBER = 'invalid_phone_number',
    SERVER_SIDE_GENERIC_ERROR = 'server_side_generic_error',
    UNEXPECTED_ERROR = 'unexpected_error'
}
