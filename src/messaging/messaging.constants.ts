export class MessagingConstants {
    public static sendOtpUrl = 'http://control.msg91.com/api/sendotp.php';
    public static verifyOtpUrl = 'https://control.msg91.com/api/verifyRequestOTP.php';
    public static messageAuthKey = '270403AjcFUbH25m0t5ca2cb0f';
    public static message = 'Your authentication code for drop is ##OTP##';
    public static sender = 'DROPUP';
    public static jwtPrivateKey = '270403AjcFUbH25m0t5ca2cb0f270403AjcFUbH25m0t5ca2cb0f';
}

export enum Country {
    INDIA = '+91',
}

export const SUCCESS = 'success';
