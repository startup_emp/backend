import { MessagingConstants } from './messaging.constants';

export class Authentication {
  public static getSendOtpUrl(): string {
    return `${MessagingConstants.sendOtpUrl}`;
  }

  public static getVerifyOtpUrl(): string {
    return `${MessagingConstants.verifyOtpUrl}`;
  }
}

/* ?authkey=${AuthenticationConstants.messageAuthKey}
&message=${AuthenticationConstants.message}
&sender=${AuthenticationConstants.sender}
&mobile=${phone}`; */
