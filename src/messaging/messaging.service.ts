import Axios, { AxiosRequestConfig } from 'axios';
import { Authentication } from '../messaging/messaging';
import { MessagingConstants, Country, SUCCESS } from '../messaging/messaging.constants';
import { ErrorConstants } from '../api/error.constants';

export class MessagingService {
  private phoneNumber: number;

  constructor(phoneNumber: number) {
    if (this.isValidNumber(phoneNumber)) {
      this.phoneNumber = phoneNumber;
    } else {
      throw new Error(ErrorConstants.INVALID_PHONE_NUMBER);
    }
  }

  private isValidNumber(phoneNumber: number): boolean {
    if (phoneNumber.toString().length !== 10) {
      return false;
    }
    return true;
  }

  public async sendOtp(): Promise<string> {
    const options: AxiosRequestConfig = {
      method: 'get',
      params: {
        authkey: MessagingConstants.messageAuthKey,
        message: MessagingConstants.message,
        mobile: Country.INDIA + this.phoneNumber,
        sender: MessagingConstants.sender
      },
      url: Authentication.getSendOtpUrl()
    };
    const message = await Axios.request(options);
    const messageResponse = message.data;
    // return new Promise((resolve: (response: string) => void, reject: (error: Error) => void) => {
    if (messageResponse.type === SUCCESS) {
      return messageResponse.type;
    } else {
      throw new Error(messageResponse.type);
    }
  }

  public async verifyOtp(otp: number): Promise<boolean> {
    const options: AxiosRequestConfig = {
      method: 'post',
      params: {
        authkey: MessagingConstants.messageAuthKey,
        mobile: Country.INDIA + this.phoneNumber,
        otp
      },
      url: Authentication.getVerifyOtpUrl()
    };
    const message = await Axios.request(options);
    const messageResponse = message.data;
    if (messageResponse.type === SUCCESS) {
      return true;
    } else {
      throw new Error(messageResponse.message);
    }
  }
}
