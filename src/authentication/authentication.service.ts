import { Promise as bluebirdPromise, resolve } from 'bluebird';
import { sign as callbackSign } from 'jsonwebtoken';
import { Response } from 'express-serve-static-core';

import { MessagingService } from '../messaging/messaging.service';
import { StatusCodes } from '../api/status.codes';
import { UsersService } from '../users/users.service';
import { UserInterface } from '../users/interfaces/user.interface';
import { LoginResponse } from '../main/interfaces/loing-response.interface';
import { SuccessResponse } from '../api/success.response';
import { ErrorResponse } from '../api/error.response';
import { ErrorTitles } from '../api/error-titles.enum';
import { ErrorConstants } from '../api/error.constants';
import { SecurityConstants } from './authentication.constants';

export class AuthenticationService {
  private async createToken(phoneNumber: number): Promise<string> {
    const sign = bluebirdPromise.promisify(callbackSign);
    return (await sign(
      { phone_number: phoneNumber },
      SecurityConstants.secretKey
    )) as string;
  }

  public async login(
    phoneNumber: number,
    otp: number,
    res: Response
  ): Promise<void> {
    const requestUrl = 'login';
    try {
      const messagingService = new MessagingService(phoneNumber);
      const isVerifiedUser = await messagingService.verifyOtp(otp); // only returns true, and throws error in false case
      if (isVerifiedUser) {
        const statusCode = StatusCodes.OK;
        const userService = new UsersService();
        let userObject = await userService.findUserByPhoneNumber(phoneNumber);
        if (!userObject) {
          userObject = await userService.addNewUser(phoneNumber);
        }
        const user = userObject.get({ plain: true }) as UserInterface;
        if (user) {
          const token = await this.createToken(phoneNumber);
          const response: LoginResponse = { token };
          new SuccessResponse(statusCode, response).sendResponse(res);
        } else {
          new ErrorResponse(
            requestUrl,
            ErrorTitles.SERVER_SIDE_GENERIC_ERROR
          ).sendResponse(res);
        }
      }
    } catch (e) {
      const err: Error = e;
      console.log(e);
      switch (err.message) {
        case ErrorConstants.INVALID_PHONE_NUMBER:
          new ErrorResponse(
            requestUrl,
            ErrorTitles.INVALID_PHONE_NUMBER
          ).sendResponse(res);
          break;
        // TODO: handle already_verified, invalid_otp cases from MessageService.verifyOtp
        default:
          new ErrorResponse(
            requestUrl,
            ErrorTitles.SERVER_SIDE_GENERIC_ERROR
          ).sendResponse(res);
      }
    }
  }

  public async validateToken(jwtToken: string): Promise<boolean> {

      return true;
  }
}
