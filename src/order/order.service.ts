import { Order } from './model/order.model';

export class OrderService {
    public async updateTransactionToken({ orderId, transactionToken }: { orderId: number; transactionToken: string; }): Promise < Order > {
        return new Promise((resolve: (response: Order) => void, reject: (error: Error) => void) => {
            Order.update(
                {transaction_token: transactionToken},
                {returning: true, where: {
                    id: orderId
                }}
            )
            .then(([, [updatedOrder] ]) => resolve(updatedOrder));
        });
    }

    public async createOrder(phoneNumber: number, quantity: number, deliveryTime: number): Promise < Order > {
        return new Promise((resolve: (response: Order) => void, reject: (error: Error) => void) => {
            Order.create({
                phone: phoneNumber,
                quantity,
                delivery_time: deliveryTime
            })
            .then(orderObject => resolve(orderObject))
            .catch(e => reject(e));
        });
    }
}
