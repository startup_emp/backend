export interface OrderResponseInterface {
    order_id: number;
    txn_token: string;
}
