export enum OrderStatus {
    CREATED = 1,
    PROCESSING = 2,
    COMPLETED = 3
}
