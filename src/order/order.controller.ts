import { Response } from 'express-serve-static-core';

import { OrderService } from '../order/order.service';
import { PaytmService } from '../payments/paytm/paytm.service';
import { OrderInterface } from '../order/model/order.model';
import { OrderResponseInterface } from '../order/interface/order.interface';
import { UsersService } from '../users/users.service';

import { UserInterface } from '../users/interfaces/user.interface';
import { SuccessResponse } from '../api/success.response';
import { StatusCodes } from '../api/status.codes';

export class OrderController {
  public async createOrder(
    phoneNumber: number,
    quantity: number,
    deliveryTime: number,
    res: Response
  ) {
    try {
      const orderService = new OrderService();
      const paytmService = new PaytmService();
      const orderObject = await orderService.createOrder(
        phoneNumber,
        quantity,
        deliveryTime
      );
      const order = orderObject.get({ plain: true }) as OrderInterface;
      const userService = new UsersService();
      const userObject = await userService.findUserByPhoneNumber(phoneNumber);
      if (!userObject) {
        throw new Error('USER_NOT_FOUND');
      }
      const user = userObject.get({ plain: true }) as UserInterface;
      const transactionToken = await paytmService.initiateTransaction(
        order.id,
        user
      );
      const orderId = order.id;
      const response: OrderResponseInterface = {
        order_id: orderId,
        txn_token: transactionToken
      };
      new SuccessResponse(StatusCodes.CREATED, response).sendResponse(res);
      orderService.updateTransactionToken({ orderId, transactionToken });
    } catch (e) {
      const err: Error = e;
    }
  }

  public async createChecksum() {
    const paytmService = new PaytmService();
    paytmService.generateChecksum()
  }
}
