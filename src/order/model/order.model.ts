import {Table, Column, Model} from 'sequelize-typescript';
import { DataTypes } from 'sequelize';
import { OrderStatus } from '../constants/order.constants';

@Table({
    timestamps: false,
    freezeTableName: true
})
export class Order extends Model<Order> {

    @Column({ type: DataTypes.BIGINT, field: 'phone' })
    phoneNumber!: number;

    @Column(DataTypes.TINYINT)
    quantity!: number;

    @Column({ type: DataTypes.BIGINT, field: 'delivery_time' })
    deliveryTime!: number;

    @Column(DataTypes.TINYINT)
    status!: OrderStatus;
}

export interface OrderInterface {
    id: number;
    phoneNumber: number;
    quantity: string;
    deliveryTime: string;
    status: OrderStatus;
}
